# Kubernetes Research

## Goal: 

A way to specify both our infrastructure and kubernetes deployment declaritively so that we can more easily deploy infrastructure and a K8s cluster independent of the service provider. 

## Technologies:

### Kubespray

https://github.com/kubernetes-sigs/kubespray

> Kubespray runs on bare metal and most clouds, using Ansible as its substrate for provisioning and orchestration
> For people with familiarity with Ansible, existing Ansible deployments or the desire to run a Kubernetes cluster across multiple platforms, Kubespray is a good choice.
> 
### Kubespray vs kubeadm

> Kubeadm provides domain Knowledge of Kubernetes clusters' life cycle management, including self-hosted layouts, dynamic discovery services and so on. Had it belonged to the new operators world, it may have been named a "Kubernetes cluster operator". Kubespray however, does generic configuration management tasks from the "OS operators" ansible world, plus some initial K8s clustering (with networking plugins included) and control plane bootstrapping.

> Kubespray has started using kubeadm internally for cluster creation since v2.3 in order to consume life cycle management domain knowledge from it and offload generic OS configuration things from it, which hopefully benefits both sides.

(https://github.com/kubernetes-sigs/kubespray/blob/master/docs/comparisons.md)

Kubespray is built to deploy kuberntes clusters, it supports: 

- support for deployments on AWS, Google Compute Engine, Microsoft Azure, OpenStack, and bare metal
- deployment of Kubernetes highly available clusters
- a composable architecture with a choice from six network plugins
- support for a variety of Linux distributions
- support for continuous integration tests
- kubeadm under the hood

> The challenge with using Kubespray, as noted by Sergey, is that it does not automatically create virtual machines. However, this issue may be resolved by leveraging the supported infrastructure management tools, such as Terraform.

(https://www.altoros.com/blog/a-multitude-of-kubernetes-deployment-tools-kubespray-kops-and-kubeadm/)

Kuberspray should provide the capability we need to customize our kubernetes deployments to our own needs. It is a popular framework and officially supported by Kubernetes. 

### Terraform

https://github.com/hashicorp/terraform

Terraform offers a way to declare infrastructure as code, and uses the APIs of multiple cloud providers to allow deployments specification independent of the provider. 


> I've been using Kubespray for the past few years. It works great. Terraform for provisioning, Kubespray/Ansible for configuration management, and helm+Kustomize for in-cluster work.

-- reddit comment
